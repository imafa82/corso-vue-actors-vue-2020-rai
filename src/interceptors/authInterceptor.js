import store from '../store'

export default (request, next) => {
    const token = store.getters.getToken;
    if(token){
        request.headers.set('Authorization', token);
    }
    next(res => {
        if(res.status === 401)
            store.dispatch('logout');
    });
};
