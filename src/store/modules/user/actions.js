import Vue from 'vue';
import * as actionType from './actionType';
import * as mutationType from './mutationType';

const actions = {
    [actionType.setUsers]: (context, payload) => {
        context.commit(mutationType.SET_USERS, payload)
    },
    [actionType.setUsersCall]: (context) => {
        Vue.http.get(`${process.env.VUE_APP_URL}users`).then(res => {
            context.dispatch(actionType.setUsers, res.body);
        })
    }
}

export default actions;
