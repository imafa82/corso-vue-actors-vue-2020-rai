import * as mutationType from './mutationType';
const mutations = {
    [mutationType.SET_USERS]: (state, payload = []) => state.users = [...payload],
};

export default mutations;
