import * as mutationType from './mutationType';
const mutations = {
    [mutationType.SET_TITLE]: (state, payload) => state.title = payload,
};

export default mutations;
