import * as actionType from './actionType';
import * as mutationType from './mutationType';

const actions = {
    [actionType.setTitle]: (context, payload) => {
        context.commit(mutationType.SET_TITLE, payload)
    }
}

export default actions;
