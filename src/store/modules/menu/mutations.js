import * as mutationType from './mutationType';
const mutations = {
    [mutationType.SET_VOICES]: (state, payload = []) => state.voices = [...payload],
    [mutationType.RESET_VOICES]: (state) => state.voices = [],
};

export default mutations;
