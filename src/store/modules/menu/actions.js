import routes from '../../../router/authRoutes';
import * as actionType from './actionType';
import * as mutationType from './mutationType';

const actions = {
    [actionType.setVoices]: (context, payload) => {
        context.commit(mutationType.SET_VOICES, payload)
    },
    [actionType.setMenuAuth]: context => {
        const voices = routes.filter(route => route.menuTitle);
        context.dispatch(actionType.setVoices, voices);
    },
    [actionType.resetVoices]: context => {
        context.commit(mutationType.RESET_VOICES);
    }
}

export default actions;
