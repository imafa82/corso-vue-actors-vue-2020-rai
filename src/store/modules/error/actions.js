import * as actionType from './actionType';
import * as mutationType from './mutationType';

const actions = {
    [actionType.addError]: (context, payload) => {
        const error = {...payload, id: Date.now() * (Math.round(Math.random() * 100))};
        context.commit(mutationType.ADD_ERROR, error)
    },
    [actionType.removeError]: (context, payload) => {
        context.commit(mutationType.REMOVE_ERROR, payload.id)
    }
}

export default actions;
