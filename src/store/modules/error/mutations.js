import * as mutationType from './mutationType';
const mutations = {
    [mutationType.ADD_ERROR]: (state, payload) => state.errors = [...state.errors, payload],
    [mutationType.REMOVE_ERROR]: (state, payload) => {
        state.errors = state.errors.filter(error => error.id !== payload);
    }

};

export default mutations;
