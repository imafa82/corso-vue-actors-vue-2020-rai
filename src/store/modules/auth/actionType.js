export const autoLogin = 'autoLogin';
export const setUser = 'setUser';
export const setLoggedUser = 'setLoggedUser';
export const setToken = 'setToken';
export const loginCall = 'loginCall';
export const setLogin = 'setLogin';
export const logout = 'logout';
