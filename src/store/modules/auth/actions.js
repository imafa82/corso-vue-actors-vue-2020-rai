import Vue from 'vue';
import router from '../../../router';
import * as actionType from './actionType';
import * as mutationType from './mutationType';
import * as menuActionType from '../menu/actionType';
import * as errorActionType from '../error/actionType';
const actions = {
    [actionType.autoLogin]: context => {
      if(localStorage.getItem('token')
          && (!context.state.token || !context.state.user) ){
          context.dispatch(actionType.setToken, localStorage.getItem('token'));
          context.dispatch(actionType.setLoggedUser);
      }
    },
    [actionType.setUser]: (context, payload) => {
        context.commit(mutationType.SET_USER, payload);
        context.dispatch(menuActionType.setMenuAuth);
    },
    [actionType.setLoggedUser]: context => {
        Vue.http.get(`${process.env.VUE_APP_URL}users/logged`).then(res => {
            context.dispatch(actionType.setUser, res.body);
        })
    },
    [actionType.setToken]: (context, payload) => {
        context.commit(mutationType.SET_TOKEN, payload)
    },
    [actionType.loginCall]: (context, payload) => {
         Vue.http.post(`${process.env.VUE_APP_URL}login`, payload)
            .then(res => {
                if(res.ok){
                    context.dispatch(actionType.setLogin, res.body);
                    router.push({name: 'Home'});
                }
            }, err => {
                context.dispatch(errorActionType.addError, {message: 'Errore in fase di Login'});
                context.dispatch(errorActionType.addError, {message: 'Errore2 in fase di Login'});
            })
    },
    [actionType.setLogin]: (context, payload) => {
        localStorage.setItem('token', payload.token);
        context.dispatch(actionType.setToken, payload.token);
        context.dispatch(actionType.setUser, payload.user);
    },
    [actionType.logout]: context => {
        localStorage.removeItem('token');
        context.commit(mutationType.DELETE_USER);
        context.commit(mutationType.DELETE_TOKEN);
        context.dispatch(menuActionType.resetVoices);
        router.push({name: 'Login'})
    }
}

export default actions;
