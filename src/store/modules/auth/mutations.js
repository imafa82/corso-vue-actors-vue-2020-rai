import * as mutationType from './mutationType';

const mutations = {
    [mutationType.SET_TOKEN]: (state, payload) => state.token = payload,
    [mutationType.DELETE_TOKEN]: state => state.token = undefined,
    [mutationType.SET_USER]: (state, payload) => state.user = {...payload},
    [mutationType.DELETE_USER]: state => state.user = undefined
};

export default mutations;
