const getters = {
    getActors: state => state.actors,
    getSelectedActor: state => state.selectedActor,
}

export default getters;
