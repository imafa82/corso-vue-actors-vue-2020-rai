export const SET_ACTORS = 'SET_ACTORS';
export const SET_SELECTED_ACTOR = 'SET_SELECTED_ACTOR';
export const ADD_ACTOR = 'ADD_ACTOR';
export const UPDATE_ACTOR = 'UPDATE_ACTOR';
export const DELETE_ACTOR = 'DELETE_ACTOR';
