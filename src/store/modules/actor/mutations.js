import * as mutationType from './mutationType';
const mutations = {
    [mutationType.SET_ACTORS]: (state, payload = []) => state.actors = payload.map(actor => ({...actor})),
    [mutationType.SET_SELECTED_ACTOR]: (state, payload) => state.selectedActor = {...payload},
    [mutationType.ADD_ACTOR] : (state, payload) => state.actors = [...state.actors, payload],
    [mutationType.UPDATE_ACTOR]: (state, payload) => state.actors = state.actors.map(actor => actor._id === payload._id ? {...actor, ...payload} : actor),
    [mutationType.DELETE_ACTOR]: (state, payload) => state.actors = state.actors.filter(actor => actor._id !== payload)
};

export default mutations;
