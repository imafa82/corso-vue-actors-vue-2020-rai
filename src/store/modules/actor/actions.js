import Vue from 'vue';
import router from '../../../router';
import * as actionType from './actionType';
import * as mutationType from './mutationType';
import * as errorActionType from '../error/actionType';
const actions = {
    [actionType.setActors]: (context, payload) => {
        context.commit(mutationType.SET_ACTORS, payload)
    },
    [actionType.setActorsCall]: (context) => {
        if(!context.state.actors || !context.state.actors.length){
            Vue.http.get(`${process.env.VUE_APP_URL}actors`).then(res => {
                context.dispatch(actionType.setActors, res.body);
            }, err => {
                context.dispatch(errorActionType.addError, {message: 'Errore durante il caricamento degli attori'})
            })
        }

    },
    [actionType.setSelectedActor]: (context, payload) => {
        context.commit(mutationType.SET_SELECTED_ACTOR, payload)
    },
    [actionType.setActorCall]: (context, payload) => {
        Vue.http.get(`${process.env.VUE_APP_URL}actors/${payload}`).then(res => {
            context.dispatch(actionType.setSelectedActor, res.body);
        }, err => {
            context.dispatch(errorActionType.addError, {message: err.body.message})
        })
    },
    [actionType.addActor]: (context, payload) => {
        context.commit(mutationType.ADD_ACTOR, payload)
    },
    [actionType.addActorCall] : (context, payload) => {
        Vue.http.post(`${process.env.VUE_APP_URL}actors`, payload).then(res => {
            //context.dispatch(actionType.addActor, res.body);
            router.push({name: 'Actors'});
        }, err => {
            context.dispatch(errorActionType.addError, {message: err.body.message})
        })
    },
    [actionType.updateActor]: (context, payload) => {
        context.commit(mutationType.UPDATE_ACTOR, payload);
    },
    [actionType.updateActorCall]: (context, payload) => {
        Vue.http.patch(`${process.env.VUE_APP_URL}actors/${payload._id}`, payload).then(res => {
            context.dispatch(actionType.updateActor, payload);
            router.push({name: 'Actor', params:{id: payload._id}});
        })
    },
    [actionType.deleteActor]: (context, payload) => {
        context.commit(mutationType.DELETE_ACTOR, payload);
    },
    [actionType.deleteActorCall]: (context, payload)=> {
        Vue.http.delete(`${process.env.VUE_APP_URL}actors/${payload}`).then(res => {
           context.dispatch(actionType.deleteActor, payload);
        });
    },
    [actionType.addActorSocket]: (context, payload) => {
        context.state.actors && context.state.actors.length ?
            context.dispatch(actionType.addActor, payload) : null;
    }
}

export default actions;
