import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import menu from './modules/menu'
import error from './modules/error'
import user from './modules/user'
import actor from './modules/actor'
import page from './modules/page'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    menu,
    error,
    user,
    actor,
    page
  }
})
