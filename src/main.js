import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import App from './App.vue'
import VueResource from 'vue-resource'
import router from './router'
import store from './store'
import authInterceptor from "./interceptors/authInterceptor";
import CoreuiVue from '@coreui/vue';
import socketService from './services/socketService';

Vue.config.productionTip = false
Vue.use(BootstrapVue);
Vue.use(CoreuiVue);
Vue.use(VueResource);
Vue.http.interceptors.push(authInterceptor);
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
