import authRoutes from "./authRoutes";
import AuthGuard from "../guards/authGuard";
import NoAuthGuard from "../guards/NoAuthGuard";


const routes = [
    {
        path: '/',
        name: 'Login',
        beforeEnter: NoAuthGuard,
        component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
    },
    {
        path: '/admin',
        beforeEnter: AuthGuard,
        component: () => import(/* webpackChunkName: "auth" */ '../views/AuthPage.vue'),
        children: authRoutes
    }
];

export default routes;
