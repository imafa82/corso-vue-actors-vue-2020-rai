const authRoutes = [
    {
        path: '/',
        name: 'Home',
        menuTitle: 'Home',
        component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
    },
    {
        path: '/users',
        name: 'Users',
        menuTitle: 'Utenti',
        component: () => import(/* webpackChunkName: "users" */ '../views/Users.vue')
    },
    {
        path: '/actors',
        name: 'Actors',
        menuTitle: 'Attori',
        component: () => import(/* webpackChunkName: "actors" */ '../views/actors/Actors.vue')
    },
    {
        path: '/actors/create',
        name: 'ActorStore',
        component: () => import(/* webpackChunkName: "actor-store" */ '../views/actors/ActorStore.vue')
    },
    {
        path: '/actors/:id',
        name: 'Actor',
        component: () => import(/* webpackChunkName: "actor" */ '../views/actors/Actor.vue')
    },
    {
        path: '/actors/:id/edit',
        name: 'ActorEdit',
        component: () => import(/* webpackChunkName: "actor-edit" */ '../views/actors/ActorEdit.vue')
    },

];

export default authRoutes;
