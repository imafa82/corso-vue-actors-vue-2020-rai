import io from 'socket.io-client';
import store from '../store';
import * as actorActionType from '../store/modules/actor/actionType';
class SocketService {
    constructor(){
        this.socket = io('http://localhost:2000');
        this.socket.on('message', data => {
            console.log(data);
            alert(data);
        });
        this.socket.on('addActor', data => {

            store.dispatch(actorActionType.addActorSocket, data);
        });
    }

    subscribeActor(){
        let token = localStorage.getItem('token');
        this.socket.emit('actorsRoom', {token});
    }
}

export default new SocketService();
